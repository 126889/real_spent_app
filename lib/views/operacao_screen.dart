import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:real_spent_app/components/rounded_button.dart';
import 'package:real_spent_app/constants.dart';
import 'package:real_spent_app/model/Operacao.dart';
import 'package:real_spent_app/model/Usuario.dart';
import 'package:real_spent_app/globals.dart' as globals;
import 'package:real_spent_app/util/funcoes.dart';
import 'package:real_spent_app/views/home_screen.dart';

import '../model/ScreenArguments.dart';

class OperacaoScreen extends StatefulWidget {
  static const String id = '/operacao';
  @override
  _OperacaoScreenState createState() => _OperacaoScreenState();
}

String dropdownValue = 'Selecione o tipo';
String dropdownValueCat = 'Selecione a categoria';

Color dropColor = kSecondColor;
Color dropColorCat = kSecondColor;

//List<String> categorias = Categoria.getCategorias();

Operacao operacao = null;
int c = 0; //Contador para controlar se dropdown já foi utilizado
bool repetir = false;
int qtdRepetir = 0;

class _OperacaoScreenState extends State<OperacaoScreen> {
  @override
  void initState() {
    //categorias = Categoria.getCategorias();
    c = 0;
    operacao = null;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;
    if (args == null) {
      operacao = operacao == null ? Operacao.vazio() : operacao;

      if (c == 0) {
        kTextMoeda.text = "0,00";
        dropdownValue = "Selecione o tipo";
        dropdownValueCat = 'Selecione a categoria';
        dropColor = kSecondColor;
      }
    } else {
      operacao = args.operacao;
      kTextMoeda.text = operacao.valor;
      dropdownValue = operacao.tipo;
      dropdownValueCat = operacao.categoria;
      if (operacao.tipo == "Entrada") {
        dropColor = Colors.lightGreen;
      } else {
        dropColor = Colors.redAccent;
      }
    }

    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 30.0,
                ),
                DropdownButton<String>(
                  isExpanded: true,
                  value: dropdownValue,
                  icon: Icon(
                    Icons.arrow_downward,
                  ),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(color: kSecondColor),
                  underline: Container(
                    height: 2,
                    color: dropColor,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      if (newValue == "Saída") {
                        dropColor = Colors.redAccent;
                      } else if (newValue == "Entrada") {
                        dropColor = Colors.lightGreen;
                      } else {
                        dropColor = kSecondColor;
                      }
                      dropdownValue = newValue;
                      c++;
                      operacao.tipo = newValue;
                    });
                  },
                  items: <String>['Selecione o tipo', 'Entrada', 'Saída']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),

                SizedBox(
                  height: 20.0,
                ),
                DropdownButton<String>(
                  isExpanded: true,
                  value: dropdownValueCat,
                  icon: Icon(
                    Icons.arrow_downward,
                  ),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(color: kSecondColor),
                  underline: Container(
                    height: 2,
                    color: dropColorCat,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValueCat = newValue; //CategoriaDrop
                      operacao.categoria = newValue;
                    });
                  },
                  items: globals.gCategorias
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  initialValue:
                      operacao.descricao == null ? "" : operacao.descricao,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    //debugger();
                    operacao.descricao = value;
                  },
                  decoration: kInputDecoration.copyWith(hintText: 'Descrição'),
                ),

                SizedBox(
                  height: kMarginInput,
                ),
                TextFormField(
                  controller: kTextMoeda,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    operacao.valor = doubleToString(kTextMoeda.numberValue);
                  },
                  keyboardType: TextInputType.number,
                  decoration: kInputDecoration.copyWith(hintText: 'Valor'),
                ),
                SizedBox(
                  height: kMarginInput,
                ),

                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Material(
                        color: kSecondColor,
                        borderRadius: BorderRadius.circular(30.0),
                        elevation: 5.0,
                        child: MaterialButton(
                          onPressed: () {
                            setState(() {
                              repetir = !repetir;
                            });
                          },
                          minWidth: 150.0,
                          height: 30.0,
                          child: Text(
                            "Repetir mensalmente",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: repetir,
                      child: SizedBox(
                        height: 50,
                        width: 140,
                        child: TextFormField(

                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: kInputDecoration.copyWith(hintText: 'Max.: 12'),
                          onChanged: (value) {
                            //debugger();

                            qtdRepetir = int.parse(value)>12?12:int.parse(value);
                            
                          },

                        ),
                      ),
                    ),

                  ],
                ),

                RoundedButton(kSecondColor, 'Cadastrar', () async {
                  var dataAtual = DateTime
                      .now(); //TODO: Permitir escolha da data pelo usuário

                  if(!repetir){

                    operacao.dataHora = dataAtual.year.toString() +
                        "." +
                        zeroAEsquerda(dataAtual.month) +
                        "." +
                        zeroAEsquerda(dataAtual.day) +
                        "." +
                        zeroAEsquerda(dataAtual.hour) +
                        "." +
                        zeroAEsquerda(dataAtual.minute);

                    operacao.usuario = auth.currentUser.email;
                    if (args == null) {
                      await operacao.addOperacao(operacao);
                    } else {
                      await Operacao.editarOperacao(args.id, operacao);
                    }
                    c = 0;

                    Navigator.pop(context);

                    Navigator.pushNamed(context, Home_screen.id);

                  }else{
                    int ano = 0;
                    int mes = 0;
                    for(int i=0;i<qtdRepetir;i++){


                      ano = dataAtual.month+i == 13? ano+1 : ano;
                      mes = dataAtual.month+i <13?dataAtual.month+i : dataAtual.month+i-12;


                      operacao.dataHora = zeroAEsquerda(dataAtual.year+ano) +
                          "." +
                          zeroAEsquerda(mes) +
                          "." +
                          zeroAEsquerda(dataAtual.day) +
                          "." +
                          zeroAEsquerda(dataAtual.hour) +
                          "." +
                          zeroAEsquerda(dataAtual.minute);

                      operacao.usuario = auth.currentUser.email;
                      if (args == null) {
                        await operacao.addOperacao(operacao);
                      } else {
                        await Operacao.editarOperacao(args.id, operacao);
                      }
                      c = 0;

                      Navigator.pop(context);

                      Navigator.pushNamed(context, Home_screen.id);
                    }
                  }
                }),
                //
              ],
            ),
          ],
        ),
      ),
    );
  }
}
