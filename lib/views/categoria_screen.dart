import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:real_spent_app/components/rounded_button.dart';
import 'package:real_spent_app/constants.dart';
import 'package:real_spent_app/model/Categoria.dart';
import 'package:real_spent_app/model/Usuario.dart';
import '../model/ScreenArguments.dart';
import 'package:real_spent_app/globals.dart' as globals;
import 'package:real_spent_app/util/funcoes.dart';
import 'package:real_spent_app/views/home_screen.dart';

class CategoriaScreen extends StatefulWidget {
  static const String id = '/categoria';
  @override
  _CategoriaScreenState createState() => _CategoriaScreenState();
}

Categoria categoria = null;

class _CategoriaScreenState extends State<CategoriaScreen> {
  @override
  void initState() {
    categoria = null;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ScreenArguments args = ModalRoute.of(context).settings.arguments;
    if (args == null) {
      categoria = categoria == null ? Categoria() : categoria;
    } else {
      //categoria = args.categoria;
    }

    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 30.0,
                ),

                TextFormField(
                  initialValue:
                      categoria.descricao == null ? "" : categoria.descricao,
                  textAlign: TextAlign.center,
                  onChanged: (value) {
                    //debugger();
                    categoria.descricao = value;
                  },
                  decoration: kInputDecoration.copyWith(hintText: 'Descrição'),
                ),

                SizedBox(
                  height: kMarginInput,
                ),
                SizedBox(
                  height: kMarginInput,
                ),
                RoundedButton(kSecondColor, 'Cadastrar', () async {
                  if (args == null) {
                    await categoria.addCategoria(categoria.descricao);
                  } else {
                    await Categoria.editarCategoria(args.id, categoria);
                  }
                  Navigator.pop(context);

                  Navigator.pushNamed(context, Home_screen.id);
                }),
                //
              ],
            ),
          ],
        ),
      ),
    );
  }
}
